// vue-quill-editor配置
const Address = require('./url');
import {Error} from './notification';
// toolbar工具栏的工具选项（默认展示全部）
const toolOptions = [
  ['bold', 'italic', 'underline', 'strike'],
  ['blockquote', 'code-block'],
  [{'header': 1}, {'header': 2}],
  [{'list': 'ordered'}, {'list': 'bullet'}],
  [{'script': 'sub'}, {'script': 'super'}],
  [{'indent': '-1'}, {'indent': '+1'}],
  [{'direction': 'rtl'}],
  [{'size': ['small', false, 'large', 'huge']}],
  [{'header': [1, 2, 3, 4, 5, 6, false]}],
  [{'color': []}, {'background': []}],
  [{'font': []}],
  [{'align': []}],
  ['clean'],
  ['link', 'image', 'video']
];

//图片上传至服务器,转化为服务器路径
const handlers = {
  image: function image() {
    let self = this;
    let fileInput = this.container.querySelector('input.ql-image[type=file]');
    if (fileInput === null) {
      fileInput = document.createElement('input');
      fileInput.setAttribute('type', 'file');
      // 设置图片参数名
      fileInput.setAttribute('name', 'img');
      // 可设置上传图片的格式
      const accept = 'image/png, image/gif, image/jpeg, image/bmp, image/x-icon'; // 可选 可上传的图片格式
      fileInput.setAttribute('accept', accept);
      fileInput.classList.add('ql-image');
      fileInput.id = 'Dai';
      // 监听选择文件
      fileInput.addEventListener('change', function () {
        const file = document.getElementById("Dai").files[0];   //选择的图片文件
        const formData = new FormData();
        formData.append('file', file);
        const options = {
          method: "POST",   //请求方法
          body: formData,   //请求体
          headers: {
            'Accept': 'application/json'
          },
        };
        fetch(Address.UPLOAD, options)
          .then((response) => {
            return response.json();
          })
          .then((responseText) => {
            if (responseText.code === 0) {
              const length = self.quill.getSelection(true).index;
              //这里很重要，你图片上传成功后，img的src需要在这里添加，res.path就是你服务器返回的图片链接。
              self.quill.insertEmbed(length, 'image', responseText.data.src);
              self.quill.setSelection(length + 1);
            }
            fileInput.value = '';
          })
          .catch((error) => {
            Error(error.msg);
          })
      });
      this.container.appendChild(fileInput);
    }
    fileInput.click();
  }
};

export default {
  placeholder: '',
  theme: 'snow',  // 主题
  modules: {
    toolbar: {
      container: toolOptions,  // 工具栏选项
      handlers: handlers  // 事件重写
    }
  }
};
