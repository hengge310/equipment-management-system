export const BAIDU = "BAIDU"; //百度翻译

//post请求
export const userNameLogin = "/api/v1.Login/userNameLogin"; //帐号密码登录
export const phoneLogin = "/api/v1.Login/phoneLogin"; //手机号登录
export const forgetChange = "/api/v1.Login/forgetChange"; //重置密码
export const messageSend = "/api/v1.Login/messageSend"; //发送验证码
export const forgetCheck = "/api/v1.Login/forgetCheck"; //忘记密码身份验证
export const powerSelNoPage = "/api/v1.Powermake/powerSelNoPage"; //根据角色id所对应的权限

export const deviceOut = "/api/v1.Mydevice/deviceOut"; //设备导出

export const deviceAddAll = "/api/v1.Mydevice/deviceAddAll"; //设备导入
export const deviceList = "/api/v1.Device/deviceList"; //设备列表
export const deviceQrcode = "/api/v1.Device/deviceQrcode"; //导出二维码

// export const deviceAdd = "/api/v1.Device/deviceAdd";//设备添加
export const deviceAdd = "/api/v1.Mydevice/deviceAdd"; //设备添加
export const deviceEdit = "/api/v1.Device/deviceEdit"; //设备编辑
export const deleteDevice = "/api/v1.device/deleteDevice"; //设备删除

export const deviceDetails = "/api/v1.Device/deviceDetails"; //设备详情
export const deviceTransfer = "/api/v1.Devicedistribution/deviceTransfer"; //设备调转
export const deviceDistribution =
  "/api/v1.Devicedistribution/deviceDistribution"; //设备分发
export const deviceTypeList = "/api/v1.Device/deviceTypeList"; //设备类型列表
export const deviceTypeAdd = "/api/v1.Device/deviceTypeAdd"; //设备类型添加
export const deviceTypeEdit = "/api/v1.Device/deviceTypeEdit"; //设备类型编辑
export const deviceTypeDel = "/api/v1.Device/deviceTypeDel"; //设备类型删除
export const deviceModelList = "/api/v1.Device/deviceModelList"; //设备型号列表
export const deviceModelAdd = "/api/v1.Device/deviceModelAdd"; //设备型号添加
export const deviceModelEdit = "/api/v1.Device/deviceModelEdit"; //设备型号编辑
export const deviceModelDel = "/api/v1.Device/deviceModelDel"; //设备型号删除
export const deviceVersionList = "/api/v1.Thermostat/deviceVersionList"; //温控器版本列表
export const deviceVersionAdd = "/api/v1.Thermostat/deviceVersionAdd"; //温控器版本添加
export const deviceVersionEdit = "/api/v1.Thermostat/deviceVersionEdit"; //温控器版本编辑
export const deviceTypeDelete = "/api/v1.Thermostat/deviceTypeDelete"; //温控器版本删除
export const getMonitorList = "/api/v1.Monitor/getMonitorList"; //监控模板列表
export const monitorAdd = "/api/v1.Monitor/monitorAdd"; //监控模板添加
export const monitorEdit = "/api/v1.Monitor/monitorEdit"; //监控模板编辑
export const monitorFind = "/api/v1.Monitor/monitorFind"; //监控模板详情
export const deviceMakeQrcode = "/api/v1.Device/deviceMakeQrcode"; //生成设备二维码
export const projectlist = "/api/v1.Store/projectMenu"; //项目所对应的组和门店
export const sendF7 = "/api/v1.send_tcp/sendF7"; //温控器版本F7更新dfu 目标扇区
export const sendF2 = "/api/v1.send_tcp/sendF2"; //温控器版本F2 升级
export const sendF0 = "/api/v1.send_tcp/sendF0"; //温控器版本F0 更新版本
export const sendF3 = "/api/v1.send_tcp/sendF3"; //温控器版本F3 切换模式
export const sendF4 = "/api/v1.send_tcp/sendF4"; //温控器版本F4 更新模式
export const sendF6 = "/api/v1.send_tcp/sendF6"; //温控器版本F2 更新制冷模式
export const sendReadF1 = "/api/v1.send_tcp/sendReadF1"; //温控器版本F1 读取温控参数
export const sendFBOpen = "/api/v1.send_tcp/sendFBOpen"; //温控器版本设备开机
export const sendFBOff = "/api/v1.send_tcp/sendFBOff"; //温控器版本  设备待机
export const picAdd = "/api/v1.Version/picAdd"; //温控器版本上传bin
export const OneClickUpgrade = "/api/v1.send_tcp/OneClickUpgrade"; //温控器版本上传bin

export const getDeviceVer = "/api/v1.Device/getDeviceVer"; //根据设备id拿到数据
export const monitorDelete = "/api/v1.Monitor/monitorDelete"; //删除监控模版
export const synchroWriteF1All = "/api/v1.Send_tcp/synchroWriteF1All"; //批量配置温控参数
export const saveTheFromwork = "/api/v1.Mythermostat/saveTheFromwork"; //另存为参数模版

//下拉列表数据
export const getProject = "/api/v1.Store/getProject"; //项目下拉列表数据
export const getWhereGroup = "/api/v1.Store/getWhereGroup"; //调转组下拉列表数据,projectId项目id
export const getWhereStore = "/api/v1.Store/getWhereStore"; //调转门店下拉列表数据,groupId组id
export const devicModelList = "/api/v1.Device/devicModelList"; //设备型号下拉列表数据
export const devicTypeList = "/api/v1.Device/devicTypeList"; //设备类型下拉列表数据
export const getDeviceList = "/api/v1.Device/getDeviceList"; //设备下拉列表数据
export const getStoreList = "/api/v1.Devicedistribution/getStoreList"; //所属分支下拉列表数据
export const getVersionList = "/api/v1.Device/getVersionList"; //温控器版本下拉列表数据
export const deviceMonitorList = "/api/v1.Monitor/deviceMonitorList"; //监控项列表
export const rolesList = "/api/v1.Employees/getRoleList"; //获取角色列表
export const roleDelete = "/api/v1.role/del"; //删除角色
export const phoneGetUser = "/api/v1.store/phoneGetUser"; //根据手机查看用户信息
export const deleteEmployee = "/api/v1.Employee/deleteEmployee"; //删除工作人员

export const staffList = "/api/v1.Myemployee/getEmployeeList"; //获取员工列表
export const homeList = "/api/v1.Devicedistribution/getDeviceWorkOrder"; //获取主页工单列表
export const roleList = "/api/v1.role/getRoleList"; //获取角色无分页列表
export const addStaff = "/api/v1.Myemployee/add"; // 添加员工--项目中心
export const udateStaff = "/api/v1.Myemployee/update"; // 修改员工信息
export const statuStaff = "/api/v1.Employee/changeStatus"; // 修改员工在职状态
export const deviceInformation = "/api/v1.Device/deviceInformation"; // 获取设备基本详情
export const getStaffParticulars = "/api/v1.Myemployee/employeeDetail"; // 获取员工详细信息
// export const offLineList = "/api/v1.Monitor/offLineList" //获取离线设备列表
export const warningList = "/api/v1.Monitor/warningList"; //获取告警设备列表
// export const homeDeviceCount = "/api/v1.Devicedistribution/deviceCount" //首页设备总数量和设备正常数量
export const homeDeviceCount = "/api/v1.Mydevice/homeDeviceCount"; //左侧设备计算
export const homeAddressDevice = "/api/v1.Mydevice/homeAddressDevice"; //右侧设备计算

export const homeWarningDevice = "/api/v1.Devicedistribution/warningDevice"; //首页告警设备列表
export const historyCurveList = "/api/v1.Curve/historyCurveList"; //设备历史记录列表
export const historyCurve = "/api/v1.Curve/historyCurve"; //设备历史曲线
export const homeOffLineDevice = "/api/v1.Devicedistribution/offLineDevice"; //设备历史曲线
export const jurisdiction = "/api/v1.power/index"; //获取权限列表
export const addRoleData = "/api/v1.role/add"; //添加角色
export const addProject = "/api/v1.Store/addProject"; //添加项目组
export const distributionList2 = "/api/v1.Store/distributionList2"; // 编辑 项目门店 组 下拉接口
export const distributionList = "/api/v1.Store/distributionList"; // 项目可分配负责人员工表

export const getFreeEmployee = "/api/v1.Myemployee/getFreeEmployee"; // 获取空闲工作人员

export const menuDelete = "/api/v1.Projectmenu/menuDelete"; // 删除项目/组/门店
export const editProject = "/api/v1.Store/editProject"; // 编辑项目/组/门店
export const register = "/admin/Login/register"; //用户注册
// 工单部分
export const menuSel = "/api/v1.Store/projectMenu"; //设备分支渲染
export const orderSearch = "/api/v1.Order/orderSearch"; //工单列表
export const orderProcess = "/api/v1.Order/orderProcess"; //工单列表搜索
export const orderCount = "/api/v1.Order/orderCount"; //获取工单统计数据
export const orderDetail = "/api/v1.Order/orderDetail"; //获取工单详情
export const orderSend = "/api/v1.Order/orderSend"; //工单派发
export const serviceSearch = "/api/v1.Service/serviceSearch"; //服务商列表
export const orderPathSel = "/api/v1.Order/orderPathSel"; //工单图片视频查询
export const orderStatusChange = "/api/v1.Order/orderStatusChange"; //工单状态改变
// 保养列表---员工列表
export const maintainSearch = "/api/v1.Maintain/maintainSearch"; //保养列表
export const employeeAdd = "/api/v1.Serviceemployee/employeeAdd"; //服务商添加员工
export const employeeUpdate = "/api/v1.Serviceemployee/employeeUpdate"; //服务商编辑员工
export const employeeSearch = "/api/v1.Serviceemployee/employeeSearch"; //服务商员工列表
export const employeeDetail = "/api/v1.Serviceemployee/employeeDetail"; //服务商员工详情
export const maintainCount = "/api/v1.Maintain/maintainCount"; //保养记录设备计算
export const changeStatus = "/api/v1.Serviceemployee/employeeLeave"; //更改 员工状态
export const repairPic = "/api/v1.Repair/repairPic"; //保养记录图片

// 服务商管理
export const mySearch = "/api/v1.Service/mySearch"; //服务商列表
export const serviceAdd = "/api/v1.Service/serviceAdd"; //添加服务商
export const serviceUpdate = "/api/v1.Service/serviceUpdate"; //编辑服务商
export const serviceDel = "/api/v1.Service/serviceDel"; //保养列表
export const serviceSel = "/api/v1.Service/serviceSel"; //保养列表
export const serviceDetail = "/api/v1.Service/serviceDetail"; //服务商详情
export const chooseEmployee = "/api/v1.Service/chooseEmployee"; //服务商选择负责人
export const employeeSearchBy = "/api/v1.Serviceemployee/employeeSearchBy"; // 服务商操作员工列表
export const maintainDetail = "/api/v1.Maintain/maintainDetail"; // 保养记录详情
export const serviceRight = "/api/v1.Order/serviceRight"; // 服务商获取 服务商id
export const serviceAuto = "/api/v1.Service/serviceAuto"; // 开始服务商自动派单
export const serviceExamine = "/api/v1.Service/serviceExamine"; // 服务商审核

export const getFreeWorker = "/api/v1.Serviceemployee/getFreeWorker"; // 服务商空闲人员接口


// 维修记录
export const repairCenter = "/api/v1.Repair/repairCenter"; //维修中心 设备列表

// 维修中心
export const repairSearch = "/api/v1.Repair/repairSearch"; //维修列表
export const repairDetail = "/api/v1.Repair/repairDetail"; //保养中心详情
export const maintainCenter = "/api/v1.Maintain/maintainCenter"; //保养中心详情
export const repairCount = "/api/v1.Repair/repairCount"; //维修记录设备计算
export const repairDetailList = "/api/v1.Repair/repairDetailList"; //维修中心--保养维修记录

// 操作记录
export const operationSel = "/api/v1.Operation/operationSel"; //操作记录
export const operationOut = "/api/v1.Operation/operationOut"; //操作记录
export const projectMenu = "/api/v1.Store/projectMenu"; //设备分支渲染
export const operationUserSel = "/api/v1.Operation/operationUserSel"; //操作人下拉

// 个人设置
export const userSet = "/api/v1.Login/userSet"; //个人设置

// 首页
export const homeStore = "/api/v1.Store/homeStore"; //个人设置
export const getCurve = "/api/v1.Device/getCurve"; //曲线

// 工作人员

export const adminList = "/api/v1.Myadmin/adminList"; //后台管理员列表
export const adminAdd = "/api/v1.Myadmin/adminAdd"; //后台管理员添加
export const adminUpdate = "/api/v1.Myadmin/adminUpdate"; //后台管理员修改
export const adminDel = "/api/v1.Myadmin/adminDel"; //后台管理员列表删除
export const roleSelNoPage = "/api/v1.Powermake/roleSelNoPage"; //角色查询
export const changestatus = "/api/v1.Myadmin/changeStatus"; //修改员工状态
export const getFreeAdmin = "/api/v1.Myadmin/getFreeAdmin"; //工作人员的空闲人员

// 角色管理

export const roleSelPage = "/api/v1.Powermake/roleSelPage"; //角色列表
export const roleDel = "/api/v1.Powermake/roleDel"; //删除角色
export const roleUpdate = "/api/v1.Powermake/roleUpdate"; //删除角色
export const roleAdd = "/api/v1.Powermake/roleAdd"; //添加角色

// 权限管理
// export const powerSel = "/api/v1.Powermake/powerSel"; //权限列表
export const powerSel = "/api/v1.Powermake/powerList"; //权限列表
export const powerLength = "/api/v1.Powermake/powerLength"; //同级权限查询
export const powerAdd = "/api/v1.Powermake/powerAdd"; //添加权限
export const powerUpdate = "/api/v1.Powermake/powerUpdate"; //编辑权限
export const powerDel = "/api/v1.Powermake/powerDel"; //删除权限

// 权限分配
export const roleSelPower = "/api/v1.Powermake/roleSelPower"; //权限分配列表
export const rolePowerAdd = "/api/v1.Powermake/addAll"; //权限分配--添加
export const rolePowerdel = "/api/v1.Powermake/delAll"; //权限分配
export const powerStart = "/api/v1.Powermake/powerStart"; // 权限启用和禁用

// 用户管理
export const userSel = "/api/v1.Myuser/userSel"; // 普通用户列表
export const userDel = "/api/v1.Myuser/userDel"; // 普通用户列表删除
export const userAdd = "/api/v1.Myuser/userAdd"; // 普通用户添加
export const userUpdate = "/api/v1.Myuser/userUpdate"; // 普通用户编辑




// 参数模块
export const thermostatList = "/api/v1.Mythermostat/thermostatList"; // F1参数列表
export const thermostatAdd = "/api/v1.Mythermostat/thermostatAdd"; // F1参数添加
export const thermostatUpdate = "/api/v1.Mythermostat/thermostatUpdate"; // F1参数修改
export const thermostatDel = "/api/v1.Mythermostat/thermostatDel"; // F1参数删除

// 温度参数配置详情
export const getDeviceThermostat = "/api/v1.device/getDeviceThermostat"; // 温度参数配置列表
export const getThermostatList = "/api/v1.device/getThermostatList"; // 获取参数表(用于筛选)
export const sendWriteF1 = "/api/v1.send_tcp/sendWriteF1"; // 写入设备温度参数
export const synchroWriteF1 = "/api/v1.send_tcp/synchroWriteF1"; // 设备F1同步测试

//监控管理
export const monitorList = "/api/v1.Mymonitor/monitorList"; // 监控项列表
export const monitorAdd1 = "/api/v1.Mymonitor/monitorAdd"; // 监控项添加
export const monitorUpdate = "/api/v1.Mymonitor/monitorUpdate"; // 监控项编辑
export const monitorDel = "/api/v1.Mymonitor/monitorDel"; // 监控项删除

// 公司模块
export const companyList = "/api/v1.Company/companyList"; // 公司列表
export const companyAdd = "/api/v1.Company/companyAdd"; // 公司添加
export const companyDel = "/api/v1.Company/companyDel"; // 公司删除
export const adminSel = "/api/v1.Myadmin/adminSel"; // 后台管理员列表无分页
export const companyUpdate = "/api/v1.Company/companyUpdate"; // 公司的更新
export const companyCheck = "/api/v1.Company/companyCheck"; // 公司审核
export const companySel = "/api/v1.Company/companySel"; // 公司无分页列表
export const roleSelNoPage2 = "/api/v1.Powermake/roleSelNoPage2"; // 添加、审核公司下拉接口
export const companyDetail = "/api/v1.Company/companyDetail"; // 公司详情

// 模板列表

export const getTheFromworkList = "/api/v1.Mythermostat/getTheFromworkList"; // 模板参数列表
export const insertTheFromwork = "/api/v1.Mythermostat/insertTheFromwork"; // 模板参数添加
export const updateTheFromwork = "/api/v1.Mythermostat/updateTheFromwork"; // 模板参数更新
export const deleteTheFromwork = "/api/v1.Mythermostat/deleteTheFromwork"; // 模板参数删除
export const getFormworkSelect = "/api/v1.Mythermostat/getFormworkSelect"; // 获取模板信息(供复制使用)
export const copyTheFromwork = "/api/v1.Mythermostat/copyTheFromwork"; // 复制温控模板
export const loginOut = "/api/v1.Login/loginOut"; //登出

export const getIsCopy = "/api/v1.Mythermostat/getIsCopy"; // 判断是否有复制模板
export const createFormwork = "/api/v1.Mythermostat/createFormwork"; // 创立模板(没有可复制模板)

// 问题模块
export const getRobotQuesList = "/api/v1.robot/getRobotQuesList"; //获取问题列表
export const insertQuestion = "/api/v1.robot/insertQuestion"; //新增问题
export const updateQuestion = "/api/v1.robot/updateQuestion"; //更新问题
export const deleteQuestion = "/api/v1.robot/deleteQuestion"; //删除问题
export const comGetDetQueList = "/api/v1.robot/comGetDetQueList"; // 详情问题
export const getCommonQueList = "/api/v1.robot/getCommonQueList"; //添加问题的下拉
export const insertAnswer = "/api/v1.robot/insertAnswer"; //新增回复
export const getAnswerList = "/api/v1.robot/getAnswerList"; //查看回复
export const updateAnswer = "/api/v1.robot/updateAnswer"; //更新回复
export const deleteAnswer = "/api/v1.robot/deleteAnswer"; //删除回复
export const getPublicQueslist = "/api/v1.robot/getPublicQueslist"; //(型号)获取对应类型的公用问题
export const comGetDetQueListModel = "/api/v1.robot/comGetDetQueListModel"; //(型号)共用常见问题获取详细问题
export const queChangeStatus = "/api/v1.robot/queChangeStatus"; //(型号)切换是否使用公用问题

// 类型模块的问题
export const getRobotQuesListType = "/api/v1.robot/getRobotQuesListType"; //(类型)获取常见问题列表
export const insertQuestionType = "/api/v1.robot/insertQuestionType"; //(类型)添加问题

// 二维码 管理
export const companyServiceSel = "/api/v1.Companyservice/companyServiceSel"; //二维码列表
export const companyServiceAdd = "/api/v1.Companyservice/companyServiceAdd"; //二维码添加
export const companyServiceUpdate =
  "/api/v1.Companyservice/companyServiceUpdate"; //二维码编辑
export const companyServiceDel = "/api/v1.Companyservice/companyServiceDel"; //二维码删除

// 意见列表
export const opinionList = "/api/v1.Opinion/opinionList"; //意见列表

export const opinionDel = "/api/v1.Opinion/opinionDel"; //意见列表

export const deviceGetField = "/api/v1.Field/deviceGetField"; //表单数据

// 注册appkey
export const registerAppKey = "/api/v1.Appkey/registerAppKey"; // 注册appkey

// 程序故障订单列表
export const orderList = "/api/orderList"; // 程序故障订单列表

// 程序故障订单列表
export const changeStatusorder = "/api/changeStatus"; // 程序故障订单修改状态

// 程序故障订单添加
export const orderAdd = "/api/orderAdd"; //程序故障订单添加
// 查询appkey
export const selAppKey = "/api/v1.Appkey/appkeySel"; //查询appkey

//聊天功能
export const createChat = "/api/v1.Join_sock_group/createChat"; //websock连接--新版
export const sendChat = "/api/v1.Chat/sendChat"; //发送消息
export const getChatList = "/api/v1.chat/getChatList"; //获取用户聊天列表
export const getChatDetail = "/api/v1.chat/getChatDetail"; //获取聊天记录
export const getChangePeople = "/api/v1.chat/getChangePeople"; //获取可以转接的人员
export const changePeople = "/api/v1.chat/changePeople"; //转接人员
export const readMsg = "/api/v1.chat/readMsg"; //将消息改为已读
export const forceChangePeople = "/api/v1.chat/forceChangePeople"; //强制转接人员

// // 用户发送消息
// export const userSendMessage = "/api/userSendMessage"; //用户发送消息

// // 消息列表
// export const userMessageList = "/api/userMessageList"; //消息列表

// // 消息删除
// export const messageDelete = "/api/messageDelete"; //消息删除

// // 消息状态更新
// export const messageCheck = "/api/messageCheck"; //消息状态更新

export const getMontior = "/api/v1.device/getMontior"; //设备获取温控参数

// 监控项
export const getMonitorInfo = "/api/v1.Mymonitor/getMonitorInfo"; //监控项参数类型

// 进度条
export const getVersionProgress = "/api/v1.Device/getVersionProgress";

// 订单管理
export const getOrderList = "/api/v1.Express/getOrderList"; //订单列表
export const insertOrder = "/api/v1.Express/insertOrder"; //添加订单
export const deleteOrder = "/api/v1.Express/deleteOrder"; //删除订单
export const orderSnGetDetail = "/api/v1.Express/orderSnGetDetail"; //根据订单查看详情
export const updateOrder = "/api/v1.Express/updateOrder"; //订单修改

// 设备分发
export const orderGetDevice = "/api/v1.Express/orderGetDevice"; //订单获取可以绑定的设备
export const typeGetModel = "/api/v1.Express/typeGetModel"; //根据类型id获取型号数据
export const orderSnGetType = "/api/v1.Express/orderSnGetType"; //获取对应订单下的无分页设备类型
export const orderSnBindDevice = "/api/v1.Express/orderSnBindDevice"; //订单号绑定设备
export const getExpressGoods = "/api/v1.Express/getExpressGoods"; //设备分发的----查看设备

export const orderGetNumber = "/api/v1.Express/orderGetNumber"; //订单获取拥有的设备数量信息

//快递fenfa
export const deviceBindExpress = "/api/v1.Express/deviceBindExpress"; //设备绑定快递
export const deviceUnBindExpress = "/api/v1.Express/deviceUnBindExpress"; //设备解除绑定快递

export const getExpressInfoOther = "/api/v1.Express/getExpressInfoOther"; //获取信息(快递,签收,扫码等)

//配件商城
export const getFitList = "/api/v1.Fittings/getFitList"; //获取配件列表
export const insertFit = "/api/v1.Fittings/insertFit"; //增加配件库
export const deleteFit = "/api/v1.Fittings/deleteFit"; //删除配件库
export const updateFit = "/api/v1.Fittings/updateFit"; //更新配件库
export const getSupplierList = "/api/v1.Fittings/getSupplierList"; //供应商列表
export const fitEnDis = "/api/v1.Fittings/fitEnDis"; //配件启用/停用




// 配件类型
export const warIdGetFitList = "/api/v1.Fittings/warIdGetFitList"; //单据获取配件列表

export const getFitModelListSim = "/api/v1.Fittings/getFitModelListSim"; //获取配件型号数据(下拉使用)

export const getFitModelList = "/api/v1.Fittings/getFitModelList"; //获取配件型号数据
export const insertFitModel = "/api/v1.Fittings/insertFitModel"; //增加配件型号库
export const updateFitModel = "/api/v1.Fittings/updateFitModel"; //更新配件型号库
export const deleteFitModel = "/api/v1.Fittings/deleteFitModel"; //删除配件型号库

export const joinGroup = "/api/v1.Join_sock_group/joinGroup"; //组
export const changeGroup = "/api/v1.Join_sock_group/changeGroup"; //改变组

export const getWarningList = "/api/v1.Order/getWarningList"; //告警大列表
export const getWarningListSimple = "/api/v1.Order/getWarningListSimple"; //告警小列表

export const readWarning = "/api/v1.Order/readWarning"; //修改状态


//库存管理
export const getFitInvList = "/api/v1.Fittings_inventory/getFitInvList"; //库存管理列表
export const fitOutIn = "/api/v1.Fittings_inventory/fitOutIn"; //配件出库/入库
export const getOutInLog = "/api/v1.Fittings_inventory/getOutInLog"; //配件出库/入库

// 商城
export const userMarketList = "/api/v1.User_market/userMarketList"; //用户商城页
export const getShoppingCar = "/api/v1.User_market/getShoppingCar"; //获取购物车数据
export const changeShoppingCar = "/api/v1.User_market/changeShoppingCar"; //更改购物车商品
export const clearShoppingCar = "/api/v1.User_market/clearShoppingCar"; //清空购物车商品
export const getDelAddrList = "/api/v1.Delivery_address/getDelAddrList"; //获取收货地址列表
export const deleteDelAddr = "/api/v1.Delivery_address/deleteDelAddr"; //删除新地址
export const insertDelAddr = "/api/v1.Delivery_address/insertDelAddr"; //创建新地址
export const updatetDelAddr = "/api/v1.Delivery_address/updatetDelAddr"; //编辑新地址
export const setIsDefault = "/api/v1.Delivery_address/setIsDefault "; // 设置默认/非默认地址
export const submitOrder = "/api/v1.User_market/submitOrder "; // 购物车提交订单


// 历史订单
export const getOrdHisList = "/api/v1.User_market/getOrdHisList "; // 获取历史订单列表
export const orderDetaile = "/api/v1.User_market/orderDetail"; // 获取历史订单列表
export const getMakeInvDetail = "/api/v1.Invoice/getMakeInvDetail"; // 获取开票详情(历史订单-发票信息)




//商城订单
export const getMarOrderList = "/api/v1.Express/getMarOrderList"; // 商城订单列表

export const getOrderState = "/api/v1.Express/getOrderState"; // 订单状态下拉
export const getOrderOperLog = "/api/v1.Express/getOrderOperLog"; //商城订单状态
export const changePrice = "/api/v1.Express/changePrice"; //订单改价






// 发票
export const getInvLIst = "/api/v1.Invoice/getInvLIst "; // 获取历史订单列表
export const getInvoiceOne = "/api/v1.Invoice/getInvoiceOne"; // 获取开票信息(用于开票前判断)
export const insertInv = "/api/v1.Invoice/insertInv"; // 添加发票模版
export const makeInvoice = "/api/v1.Invoice/makeInvoice"; // 开票(总公司开票)
export const refuseInvoice = "/api/v1.Invoice/refuseInvoice"; // 拒绝开票
export const updateInv = "/api/v1.Invoice/updateInv"; // 更新发票模板
export const getInvDetail = "/api/v1.Invoice/getInvDetail"; // 获取发票详情(发票中心-详情)
