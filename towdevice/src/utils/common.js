import axios from 'axios';
//日期时间格式化 传入new Date() 则是当前时间
export const FormatDate = (value, type) => {
  window.Date.prototype.Format = function (fmt) {
    let o = {
      "M+": this.getMonth() + 1, // 月份
      "d+": this.getDate(), // 日
      "H+": this.getHours(), // 小时
      "m+": this.getMinutes(), // 分
      "s+": this.getSeconds(), // 秒
      "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
      "S": this.getMilliseconds() // 毫秒
    };
    if (/(y+)/.test(fmt))
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (let k in o)
      if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
  };
  if (type === 'date') { //返回日期
    return value.Format("yyyy-MM-dd");
  } else if (type === 'time') { //返回时间
    return value.Format("HH:mm:ss");
  } else { //返回日期时间
    return value.Format("yyyy-MM-dd HH:mm:ss");
  }
};
//多维数组转化为一维数组,arr数组,key子级数组属性名,flag是否包含父级
export const Flatten = (arr, key, flag) => {
  let res = [];
  if (Array.isArray(arr)) {
    for (let i = 0; i < arr.length; i++) {
      if (Array.isArray(arr[i][key])) {
        if (flag) res.push(arr[i]);
        res = res.concat(Flatten(arr[i][key], key, flag));
        if (flag) delete arr[i][key];
      } else {
        res.push(arr[i]);
      }
    }
  }
  return res;
};
//数字格式化,超过万显示万,超过亿显示亿,超过万亿显示万亿
export const NumberFormat = (value) => {
  let param = {};
  let k = 10000,
    sizes = ['', '万', '亿', '万亿'],
    i;
  if (value > 0) {
    if (value < k) {
      param.value = value;
      param.unit = ''
    } else {
      i = Math.floor(Math.log(value) / Math.log(k));
      param.value = ((value / Math.pow(k, i))).toFixed(2);
      param.unit = sizes[i];
    }
  } else {
    if (-value < k) {
      param.value = value;
      param.unit = ''
    } else {
      i = Math.floor(Math.log(-value) / Math.log(k));
      param.value = ((value / Math.pow(k, i))).toFixed(2);
      param.unit = sizes[i];
    }
  }
  return param;
};
//隐藏手机号中间部分
export const HidePhone = (phone, boolean) => {
  if (!phone || phone === '' || phone === null) {
    return '--'
  } else {
    //是否需要隐藏
    if (boolean) {
      // 判断是否为7位以上的纯数字
      if (Number(phone) && String(phone).length > 7) {
        let str1 = phone.toString().substr(0, 3);

        let str2 = phone.toString().substr(7, phone.length);

        return `${str1}****${str2}`;

      } else {
        return phone;
      }
    } else {
      return phone;
    }
  }
};
//实现冒泡排序
export const Sort = (arr) => {
  for (let i = 0; i < arr.length - 1; i++) {
    for (let j = 0; j < arr.length - 1 - i; j++) {
      if (arr[j] > arr[j + 1]) {
        var temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
  }
  return arr;
};
//判断两个数组完全相等
export const Equar = (a, b) => {
  // 判断数组的长度
  if (Array.isArray(a) && Array.isArray(b)) {
    if (a.length !== b.length) {
      return false
    } else {
      // 循环遍历数组的值进行比较
      for (let i = 0; i < b.length; i++) {
        if (typeof b[i] === "object") {
          if (a.indexOf(b[i].id) === -1) {
            return false
          }
        } else {
          if (a.indexOf(b[i]) === -1) {
            return false
          }
        }
      }
      return true;
    }
  }
};
//数组排序
export const compare = (property) => {
  return function (a, b) {
    let value1 = a[property];
    let value2 = b[property];
    return value1 - value2;
  }
};
//遍历循环对象,判断权限中有多少为true,动态改变width;
export const getObject = (obj) => {
  const arr = [];
  for (let key in obj) {
    if (obj[key]) {
      arr.push(obj[key]);
    }
  }
  return arr.length;
};
//导出excel
export const Excel = (url, params, name, method) => {
  let root = process.env.MAIN_ENV;
  let parm, data;
  if (!method) method = 'GET';
  if (method === 'GET') {
    data = '';
    parm = params;
  } else {
    data = params;
    parm = '';
  }
  axios({
      method: method,
      headers: {
        'Content-Type': 'application/json; application/octet-stream'
      },
      url: root + url,
      params: parm,
      data: data,
      responseType: 'blob'
    })
    .then(blob => {
      const fileName = name + '.xls';
      const linkNode = document.createElement('a');
      linkNode.download = fileName; //a标签的download属性规定下载文件的名称
      linkNode.style.display = 'none';
      linkNode.href = URL.createObjectURL(blob); //生成一个Blob URL
      document.body.appendChild(linkNode);
      linkNode.click(); //模拟在按钮上的一次鼠标单击
      URL.revokeObjectURL(linkNode.href); // 释放URL 对象
      document.body.removeChild(linkNode);
    })
    .catch(err => {})
};
