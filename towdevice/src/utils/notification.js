import {Notification} from 'element-ui';

const time = 1500;

//成功
export const Success = (message, title) => Notification({
  title: title ? title : '' + '成功',
  message: message,
  type: 'success',
  duration: time
});

//失败
export const Error = (message, title) => Notification({
  title: title ? title : '' + '失败',
  message: message,
  type: 'error',
  duration: time
});

//警告
export const Warning = (message, title) => Notification({
  title: title ? title : '' + '警告',
  message: message,
  type: 'warning',
  duration: time
});

//消息
export const Info = (message, title) => Notification({
  title: title ? title : '' + '消息',
  message: message,
  type: 'info',
  duration: time
});
