import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}; //解决点击两下出现 NavigationDuplicated {_name: "NavigationDuplicated"}
/*
  keepAlive 页面缓存
  closable  页签是否关闭
  title     页面名称,
  bar       是否是左侧菜单
  list      面包屑上级信息
 */
// 默认路由，不需要权限 NotFound
export const constantRouterMap = [{
    path: '/register',
    name: 'register',
    component: resolve => require(['@/pages/Login/Register'], resolve),
  },
  {
    path: '/',
    redirect: '/Login',
    hidden: true
  },
  {
    path: '/Login',
    name: 'Login',
    component: resolve => require(['@/pages/Login/Login'], resolve),
    meta: {
      requireAuth: true, //是否需要校验token存在
    }
  },
  {
    path: '/Index',
    component: resolve => require(['@/pages/Index'], resolve),
    children: []
  },
  //需要注意这里，404的路由一定要写在静态路由中,但捕获未定义路由配置一定要放在动态路由里
  {
    path: '/NotFound',
    name: 'NotFound',
    component: resolve => require(['@/pages/NotFound'], resolve),
    meta: {
      keepAlive: false,
      title: '404'
    },
  },
  {
    path: '/newimg',
    name: 'newimg',
    component: resolve => require(['@/pages/Ercode/newimg'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '图片',
    },
  },
];
/*
异步挂载的路由
动态需要根据权限加载的路由表
*/
export const asyncRouterMap = [{
    path: '/Home',
    name: 'Home',
    component: resolve => require(['@/pages/Home'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '首页',
    },
  },

  {
    path: '/Project',
    name: 'Project',
    redirect: "/projectMenu",
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '项目中心',
    },
  },
  {
    path: '/projectMenu',
    name: 'projectMenu',
    component: resolve => require(['@/pages/Project/projectMenu'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '项目菜单',
    },
  },
  {
    path: '/Person',
    name: 'Person',
    component: resolve => require(['@/pages/Project/personnel'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '工作人员',
    },
  },
  {
    path: '/particulars',
    name: 'particulars',
    component: resolve => require(['@/pages/Project/particulars'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '设备曲线',
    },
  },

  {
    path: '/DeviceControl',
    name: 'DeviceControl',
    redirect: "/PlatformEquipment",
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '设备管理',
    },
  },
  {
    path: '/PlatformEquipment',
    name: 'PlatformEquipment',
    component: resolve => require(['@/pages/Device/PlatformEquipment'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '平台设备管理',
    },
  },
  {
    path: '/UnallocatedEquipment',
    name: 'UnallocatedEquipment',
    component: resolve => require(['@/pages/Device/UnallocatedEquipment'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '未分发设备',
    },
  },
  {
    path: '/DeviceType',
    name: 'DeviceType',
    component: resolve => require(['@/pages/Device/DeviceType'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '设备类型',
    },
  },
  {
    path: '/EquipmentModel',
    name: 'EquipmentModel',
    component: resolve => require(['@/pages/Device/EquipmentModel'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '设备型号',
    },
  },
  {
    path: '/ThermostatVersions',
    name: 'ThermostatVersions',
    component: resolve => require(['@/pages/Device/ThermostatVersions'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '温控器版本',
    },
  },
  {
    path: '/MonitoringControl',
    name: 'MonitoringControl',
    component: resolve => require(['@/pages/Device/MonitoringControl'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '温控管理设置',
    },
  },
  {
    path: '/MaintainCenter',
    name: 'MaintainCenter',
    component: resolve => require(['@/pages/Maintain/MaintainCenter'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '维修中心',
    },
  },
  {
    path: '/OperatingRecord',
    name: 'OperatingRecord',
    component: resolve => require(['@/pages/log/OperatingRecord'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '操作记录',
    },
  },

  {
    path: '/WorkOrder',
    name: 'WorkOrder',
    redirect: "/AllwokOrder",
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '工单系统',
    },
  },
  {
    path: '/AllwokOrder',
    name: 'AllwokOrder',
    component: resolve => require(['@/pages/Working/AllwokOrder'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '所有工单',
    },
  },
  {
    path: '/Evaluate',
    name: 'Evaluate',
    component: resolve => require(['@/pages/Working/Evaluate'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '已评价工单',
    },
  },
  {
    path: '/NowokOrder',
    name: 'NowokOrder',
    component: resolve => require(['@/pages/Working/NowokOrder'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '代办工单',
    },
  },
  {
    path: '/JobignoredOrder',
    name: 'JobignoredOrder',
    component: resolve => require(['@/pages/Working/JobignoredOrder'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '已忽略工单',
    },
  },
  {
    path: '/AcceptedWorkOrder',
    name: 'AcceptedWorkOrder',
    component: resolve => require(['@/pages/Working/AcceptedWorkOrder'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '已受理工单',
    },
  },
  {
    path: '/WorkOrderConfirmed',
    name: 'WorkOrderConfirmed',
    component: resolve => require(['@/pages/Working/WorkOrderConfirmed'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '待确认工单',
    },
  },
  {
    path: '/ConfirmedWorkOrder',
    name: 'ConfirmedWorkOrder',
    component: resolve => require(['@/pages/Working/ConfirmedWorkOrder'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '已确认工单',
    },
  },
  {
    path: '/CompletedOrder',
    name: 'CompletedOrder',
    component: resolve => require(['@/pages/Working/CompletedOrder'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '已完成工单',
    },
  },
  {
    path: '/TaskDispatch',
    name: 'TaskDispatch',
    component: resolve => require(['@/pages/Working/TaskDispatch'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '任务派单',
    },
  },
  {
    path: '/ServiceProvider',
    name: 'ServiceProvider',
    component: resolve => require(['@/pages/Service/ServiceProvider'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '服务商设置',
    },
  },
  {
    path: '/MaintenanceCenter',
    name: 'MaintenanceCenter',
    component: resolve => require(['@/pages/Maintenance/MaintenanceCenter'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '保养中心',
    },
  },
  {
    path: '/personnel',
    name: 'personnel',
    component: resolve => require(['@/pages/Person/PersonnelCtrol'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '工作人员',
    },
  },
  {
    path: '/UserSetting',
    name: 'UserSetting',
    component: resolve => require(['@/pages/User/UserSetting'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '个人设置',
    },
  },

  {
    path: '/addRole',
    name: 'addRole',
    component: resolve => require(['@/pages/Project/addRole'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: false,
      title: '添加角色',
    },
  },
  // {
  //   path: '/RoleContent',
  //   name: 'RoleContent',
  //   component: resolve => require(['@/pages/Project/RoleContent'], resolve),
  //   meta: {
  //     keepAlive: false,
  //     closable: true,
  //     bar: true,
  //     title: '角色管理',
  //   },
  // },
  {
    path: '/ServerList',
    name: 'ServerList',
    component: resolve => require(['@/pages/Service/ServerList'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '服务商列表',
    },
  },
  {
    path: '/MaintenanceStaff',
    name: 'MaintenanceStaff',
    component: resolve => require(['@/pages/Service/MaintenanceStaff'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '维修员工列表',
    },
  },
  {
    path: '/BaoStaff',
    name: 'BaoStaff',
    component: resolve => require(['@/pages/Service/BaoStaff'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '保养员工列表',
    },
  },
  {
    path: '/MaintenanceRecords',
    name: 'MaintenanceRecords',
    component: resolve => require(['@/pages/Maintenance/MaintenanceRecords'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '保养记录',
    },
  },
  {
    path: '/EquipmentList',
    name: 'EquipmentList',
    component: resolve => require(['@/pages/Maintenance/EquipmentList'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '设备列表',
    },
  },

  {
    path: '/shebeiList',
    name: 'shebeiList',
    component: resolve => require(['@/pages/Maintain/shebeiList'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '设备列表',
    },
  },
  {
    path: '/MaintenanRecords',
    name: 'MaintenanRecords',
    component: resolve => require(['@/pages/Maintain/MaintenanRecords'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '维修记录',
    },
  },
  {
    path: '/MaintainDetial',
    name: 'MaintainDetial',
    component: resolve => require(['@/pages/Maintain/MaintainDetial'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '查看详情',
    },
  },
  {
    path: '/MaintainceDetial',
    name: 'MaintainceDetial',
    component: resolve => require(['@/pages/Maintenance/MaintainceDetial'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '查看详情',
    },
  },
  {
    path: '/Offcurve',
    name: 'Offcurve',
    component: resolve => require(['@/pages/Project/Offcurve'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '历史曲线',
    },
  },
  {
    path: '/PersonJob',
    name: 'PersonJob',
    component: resolve => require(['@/pages/Person/PersonJob'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '工作人员',
    },
  },
  {
    path: '/PersionRole',
    name: 'PersionRole',
    component: resolve => require(['@/pages/Person/PersionRole'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '角色管理',
    },
  },
  {
    path: '/PersionPerssion',
    name: 'PersionPerssion',
    component: resolve => require(['@/pages/Person/PersionPerssion'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '权限管理',
    },
  },
  // {
  //   path: '/PersonAddRole',
  //   name: 'PersonAddRole',
  //   component: resolve => require(['@/pages/Person/PersonAddRole'], resolve),
  //   meta: {
  //     keepAlive: false,
  //     closable: true,
  //     bar: true,
  //     title: '添加角色',
  //   },
  // },
  {
    path: '/Assignment',
    name: 'Assignment',
    component: resolve => require(['@/pages/Person/Assignment'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '权限分配',
    },
  },
  {
    path: '/UserList',
    name: 'UserList',
    component: resolve => require(['@/pages/UserManager/UserList'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '用户列表',
    },
  },
  {
    path: '/ThermostatList',
    name: 'ThermostatList',
    component: resolve => require(['@/pages/Thermostat/ThermostatList'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '参数模块',
    },
  },
  {
    path: '/Parameter',
    name: 'Parameter',
    component: resolve => require(['@/pages/Device/Parameter'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '参数配置',
    },
  },
  {
    path: '/Configuration',
    name: 'Configuration',
    component: resolve => require(['@/pages/Device/Configuration'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '监控管理配置',
    },
  },
  {
    path: '/CompanyList',
    name: 'CompanyList',
    component: resolve => require(['@/pages/Company/CompanyList'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '公司管理',
    },
  },
  {
    path: '/TemplateList',
    name: 'TemplateList',
    component: resolve => require(['@/pages/Thermostat/TemplateList'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '模板列表',
    },
  },
  {
    path: '/ThermostatList',
    name: 'ThermostatList',
    component: resolve => require(['@/pages/Thermostat/ThermostatList'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '参数列表',
    },
  },
  {
    path: '/Question',
    name: 'Question',
    component: resolve => require(['@/pages/Device/Question'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '问题列表',
    },
  },
  {
    path: '/CompanyServiceDel',
    name: 'CompanyServiceDel',
    component: resolve => require(['@/pages/Ercode/CompanyServiceDel'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '二维码列表',
    },
  },
  {
    path: '/DetialQuestion',
    name: 'DetialQuestion',
    component: resolve => require(['@/pages/Device/DetialQuestion'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '类型问题列表',
    },
  },
  {
    path: '/OpinionList',
    name: 'OpinionList',
    component: resolve => require(['@/pages/Project/OpinionList'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '意见列表',
    },
  },
  {
    path: '/workOrderlist',
    name: 'workOrderlist',
    component: resolve => require(['@/pages/ServiceManager/workOrderlist'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '设备故障列表',
    },
  },
  {
    path: '/orderDetial',
    name: 'orderDetial',
    component: resolve => require(['@/pages/ServiceManager/orderDetial'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '设备故障详情',
    },
  },
  {
    path: '/order',
    name: 'order',
    redirect: "/orderlist",
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '订单管理',
    },
  },
  {
    path: '/orderlist',
    name: 'orderlist',
    component: resolve => require(['@/pages/order/orderlist'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '订单列表',
    },
  },
  {
    path: '/orderdevice',
    name: 'orderdevice',
    component: resolve => require(['@/pages/order/orderdevice'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '设备列表',
    },
  },
  {
    path: '/kuadifenfa',
    name: 'kuadifenfa',
    component: resolve => require(['@/pages/order/kuadifenfa'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '快递分发',
    },
  },
  {
    path: '/devicefenfa',
    name: 'devicefenfa',
    component: resolve => require(['@/pages/order/devicefenfa'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '设备分发',
    },
  },
  {
    path: '/devicekuaidi',
    name: 'devicekuaidi',
    component: resolve => require(['@/pages/order/devicekuaidi'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '快递列表',
    },
  },
  {
    path: '/kuaidigetdevice',
    name: 'kuaidigetdevice',
    component: resolve => require(['@/pages/order/kuaidigetdevice'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '查看快递分发设备',
    },
  },
  {
    path: '/kuaidigetkuaidi',
    name: 'kuaidigetkuaidi',
    component: resolve => require(['@/pages/order/kuaidigetkuaidi'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '查看快递分发快递',
    },
  },
  {
    path: '/kuaidiDetial',
    name: 'kuaidiDetial',
    component: resolve => require(['@/pages/order/kuaidiDetial'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '快递信息',
    },
  },
  {
    path: '/partslist',
    name: 'partslist',
    component: resolve => require(['@/pages/parts/partslist'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '配件列表',
    },
  },
  {
    path: '/partsType',
    name: 'partsType',
    component: resolve => require(['@/pages/parts/partsType'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '配件类型',
    },
  },
  {
    path: '/shoparints',
    name: 'shoparints',
    component: resolve => require(['@/pages/Maintain/shoparints'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '查看配件',
    },
  },
  {
    path: '/Police',
    name: 'Police',
    component: resolve => require(['@/pages/Device/Police'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '报警记录',
    },
  },
  {
    path: '/stock',
    name: 'stock',
    component: resolve => require(['@/pages/Stock/stock'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '库存列表',
    },
  },
  {
    path: '/shop',
    name: 'shop',
    component: resolve => require(['@/pages/Shop/shop'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '用户商城',
    },
  },
  {
    path: '/shopDetial',
    name: 'shopDetial',
    component: resolve => require(['@/pages/Shop/shopDetial'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '订单详情',
    },
  },
  {
    path: '/invoice',
    name: 'invoice',
    component: resolve => require(['@/pages/Invoice/invoice'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '发票中心',
    },
  },
  {
    path: '/invoiceOrder',
    name: 'invoiceOrder',
    component: resolve => require(['@/pages/invoiceOrder/invoiceOrder'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '历史订单',
    },
  },
  {
    path: '/invoiceOrderDetial',
    name: 'invoiceOrderDetial',
    component: resolve => require(['@/pages/invoiceOrder/invoiceOrderDetial'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '历史订单详情',
    },
  },
  {
    path: '/shopOrder',
    name: 'shopOrder',
    component: resolve => require(['@/pages/shopOrder/shopOrder'], resolve),
    meta: {
      keepAlive: false,
      closable: true,
      bar: true,
      title: '商城订单',
    },
  },



];

//注册路由
export default new Router({
  mode: 'hash',
  scrollBehavior(to, from, savedPosition) {
    return {
      x: 0,
      y: 0
    }
  },
  routes: constantRouterMap
});
