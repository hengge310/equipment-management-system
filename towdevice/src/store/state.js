const state = {
  persistence: {
    agreement: false //用户协议勾选
  }, //需要持久化的数据
  isCollapse: true, ////左侧导航菜单,默认收缩
  home: {}, //首页信息
  visitedviews: [], //已选择页签
  role: [], //角色拥有的左侧导航权限列表
  menus: [{
      id: 1,
      parentId: 0,
      sort: 1,
      path: "/Home",
      name: "Home",
      title: "主页",
      icon: "icon-shouye"
    },
    {
      id: 2,
      parentId: 0,
      sort: 2,
      path: "/Project",
      name: "Project",
      title: "项目中心",
      icon: "icon-xiangmu",
      children: [{
          id: 3,
          parentId: 2,
          sort: 1,
          path: "/Monitoring",
          title: "实时监控"
        },
        {
          id: 4,
          parentId: 2,
          sort: 2,
          path: "/projectMenu",
          title: "项目菜单"
        },
        {
          id: 5,
          parentId: 2,
          sort: 3,
          path: "/Person",
          title: "工作人员"
        }
        // {
        //   id: 6,
        //   parentId: 2,
        //   sort: 4,
        //   path: "/RoleContent",
        //   title: "角色管理"
        // },
      ]
    },
    {
      id: 3,
      parentId: 0,
      sort: 3,
      path: "/DeviceControl",
      name: "DeviceControl",
      title: "设备管理",
      icon: "icon-shebei",
      children: [{
          id: 7,
          parentId: 3,
          sort: 1,
          path: "/PlatformEquipment",
          title: "平台设备管理"
        },
        {
          id: 8,
          parentId: 3,
          sort: 2,
          path: "/UnallocatedEquipment",
          title: "未分发设备"
        },
        {
          id: 9,
          parentId: 3,
          sort: 3,
          path: "/EquipmentModel",
          title: "设备型号"
        },
        {
          id: 10,
          parentId: 3,
          sort: 4,
          path: "/EquipmentModel",
          title: "设备型号"
        },
        {
          id: 11,
          parentId: 3,
          sort: 5,
          path: "/DeviceType",
          title: "设备类型"
        },
        {
          id: 12,
          parentId: 3,
          sort: 6,
          path: "/ThermostatVersions",
          title: "温控器版本"
        },
        {
          id: 13,
          parentId: 3,
          sort: 7,
          path: "/MonitoringControl",
          title: "监控管理设置"
        },
        {
          id: 30,
          parentId: 3,
          sort: 8,
          path: "/Police",
          title: "报警记录"
        }

      ]
    },
    {
      id: 4,
      parentId: 0,
      sort: 4,
      path: "/WorkOrder",
      name: "WorkOrder",
      title: "工单系统",
      icon: "icon-gongdan",
      children: [{
          id: 14,
          parentId: 4,
          sort: 1,
          path: "/AllwokOrder",
          title: "所有工单"
        },
        {
          id: 15,
          parentId: 4,
          sort: 2,
          path: "/NowokOrder",
          title: "代办工单"
        },
        {
          id: 16,
          parentId: 4,
          sort: 3,
          path: "/JobignoredOrder",
          title: "已忽略工单"
        },
        {
          id: 17,
          parentId: 4,
          sort: 4,
          path: "/AcceptedWorkOrder",
          title: "已受理工单"
        },
        {
          id: 18,
          parentId: 4,
          sort: 5,
          path: "/WorkOrderConfirmed",
          title: "待确认工单"
        },
        {
          id: 19,
          parentId: 4,
          sort: 6,
          path: "/ConfirmedWorkOrder",
          title: "已确认工单"
        },
        {
          id: 20,
          parentId: 4,
          sort: 7,
          path: "/CompletedOrder",
          title: "已完成工单"
        },
        {
          id: 21,
          parentId: 4,
          sort: 8,
          path: "/Evaluate",
          title: "已评价工单"
        }
      ]
    },
    {
      id: 5,
      parentId: 0,
      sort: 5,
      path: "/ServiceProvider",
      name: "ServiceProvider",
      title: "服务商设置",
      icon: "icon-fuwu",
      children: [{
          id: 12,
          parentId: 5,
          sort: 1,
          path: "/ServerList",
          title: "服务商列表"
        },
        {
          id: 13,
          parentId: 5,
          sort: 1,
          path: "/MaintenanceStaff",
          title: "操作员工"
        }
      ]
    },
    {
      id: 6,
      parentId: 0,
      sort: 6,
      path: "/MaintainCenter",
      name: "MaintainCenter",
      title: "维修中心",
      icon: "icon-weixiu",
      children: [{
          id: 14,
          parentId: 6,
          sort: 1,
          path: "/shebeiList",
          title: "设备列表"
        },
        {
          id: 15,
          parentId: 6,
          sort: 2,
          path: "/MaintenanRecords",
          title: "维修记录"
        }
      ]
    },
    {
      id: 7,
      parentId: 0,
      sort: 7,
      path: "/MaintenanceCenter",
      name: "MaintenanceCenter",
      title: "保养中心",
      icon: "icon-shiwuzhongxin_baoxiu",
      children: [{
          id: 16,
          parentId: 7,
          sort: 1,
          path: "/EquipmentList",
          title: "设备列表"
        },
        {
          id: 17,
          parentId: 7,
          sort: 2,
          path: "/MaintenanceRecords",
          title: "保养记录"
        }
      ]
    },
    {
      id: 8,
      parentId: 0,
      sort: 8,
      path: "/OperatingRecord",
      name: "OperatingRecord",
      title: "操作记录",
      icon: "icon-caozuojilu"
    },
    {
      id: 9,
      parentId: 0,
      sort: 9,
      path: "/personnel",
      name: "personnel",
      title: "权限管理",
      icon: "icon-ic_opt_feature",
      children: [{
          id: 18,
          parentId: 9,
          sort: 1,
          path: "/PersonJob",
          title: "工作人员"
        },
        {
          id: 19,
          parentId: 9,
          sort: 2,
          path: "/PersionRole",
          title: "角色管理"
        },
        {
          id: 20,
          parentId: 9,
          sort: 3,
          path: "/PersionPerssion",
          title: "权限管理"
        }
      ]
    },
    // {
    //   id: 10,
    //   parentId: 0,
    //   sort: 10,
    //   path: '/UserSetting',
    //   name: 'UserSetting',
    //   title: '个人设置',
    //   icon: 'icon-gerenshezhi',
    // },
    {
      id: 11,
      parentId: 0,
      sort: 11,
      path: "/UserList",
      name: "UserList",
      title: "用户管理",
      icon: "icon-yonghu1"
    },
    {
      id: 12,
      parentId: 0,
      sort: 12,
      path: "/TemplateList",
      name: "/TemplateList",
      title: "参数模块",
      icon: "icon-canshushezhi"
    },
    {
      id: 13,
      parentId: 0,
      sort: 13,
      path: "/CompanyList",
      name: "CompanyList",
      title: "公司管理",
      icon: "icon-gongsixinxi"
    },
    {
      id: 14,
      parentId: 0,
      sort: 14,
      path: "/CompanyServiceDel",
      name: "CompanyServiceDel",
      title: "二维码管理",
      icon: "icon-erweima"
    },
    {
      id: 22,
      parentId: 0,
      sort: 15,
      path: "/workOrderlist",
      name: "workOrderlist",
      title: "客服中心",
      icon: "icon-kefu"
    },
    {
      id: 23,
      parentId: 0,
      sort: 15,
      path: "/order",
      name: "order",
      title: "订单管理",
      icon: "icon-dingdanguanli",
      children: [{
          id: 24,
          parentId: 23,
          sort: 1,
          path: "/orderlist",
          title: "订单列表"
        },
        {
          id: 25,
          parentId: 23,
          sort: 2,
          path: "/devicefenfa",
          title: "设备分发"
        },
        {
          id: 26,
          parentId: 23,
          sort: 2,
          path: "/kuadifenfa",
          title: "快递分发"
        }
      ]
    },
    {
      id: 27,
      parentId: 0,
      sort: 16,
      path: "/parts",
      name: "parts",
      title: "配件商城",
      icon: "icon-lingpeijianweixiu",
      children: [{
          id: 28,
          parentId: 27,
          sort: 1,
          path: "/partslist",
          title: "配件列表"
        },
        {
          id: 29,
          parentId: 27,
          sort: 2,
          path: "/partsType",
          title: "配件类型"
        }
      ]
    }, {
      id: 31,
      parentId: 0,
      sort: 17,
      path: "/stock",
      name: "stock",
      title: "库存管理",
      icon: "icon-kucun",

    }, {
      id: 32,
      parentId: 0,
      sort: 18,
      path: "/shop",
      name: "shop",
      title: "用户商城",
      icon: "icon-shangcheng",

    }, {
      id: 33,
      parentId: 0,
      sort: 19,
      path: "/invoice",
      name: "invoice",
      title: "发票中心",
      icon: "icon-fapiaoguanli",
    }, {
      id: 33,
      parentId: 0,
      sort: 20,
      path: "/shopOrder",
      name: "shopOrder",
      title: "商城订单",
      icon: "icon-shangcheng",
    }, {
      id: 34,
      parentId: 0,
      sort: 21,
      path: "/invoiceOrder",
      name: "invoiceOrder",
      title: "历史订单",
      icon: "icon-lishidingdan",
    }
  ], //所有左侧导航菜单列表
  org_id: "", //公司id
  logo: "" //公司logo
};
export default state;
