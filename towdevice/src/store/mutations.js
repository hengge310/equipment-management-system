//递归拷贝state;
const deepClone = (obj) => {
  let newObj = obj instanceof Array ? [] : {};
  for (let i in obj) {
    newObj[i] = typeof obj[i] === "object" ? deepClone(obj[i]) : obj[i]
  }
  return newObj;
};
//展开或收起左侧菜单
export const COLLAPSE = state => {
  state.isCollapse = !state.isCollapse;
};
//切换公司
export const CHANGE_VENUE = (state, id, logo) => {
  state.org_id = id
  window.sessionStorage.setItem("companyId", id)
};
//存储state
export const SAVE = state => {
  const copyState = deepClone(state); // 拷贝state对象
  //存储state中初始数据,退出登录时重置
  localStorage.setItem('copyState', JSON.stringify(copyState));
  for (let key in copyState) {
    if (key !== "persistence") state[key] = copyState[key] // 递归赋值
  }
};
//退出登录
export const LOGOUT = state => {
  const copyState = JSON.parse(localStorage.getItem('copyState'));
  const data = localStorage.getItem('vuex');
  window.sessionStorage.clear(); //移除所有sessionStorage
  window.localStorage.clear(); //移除所有localStorage
  localStorage.setItem('vuex', data);
  for (let key in copyState) {
    if (key !== "persistence") state[key] = copyState[key] // 递归赋值
  }
};
//清除缓存
export const CLEAR = state => {
  const copyState = JSON.parse(localStorage.getItem('copyState'));
  window.sessionStorage.clear(); //移除所有sessionStorage÷
  window.localStorage.clear(); //移除所有localStorage
  for (let key in copyState) {
    state[key] = copyState[key] // 递归赋值
  }
};
//打开新页签--添加路由数据的方法
export const ADD_VISITED_VIEWS = (state, view) => {
  /*  some() 方法用于检测数组中的元素是否满足指定条件,
    如果有一个元素满足条件，则表达式返回true , 剩余的元素不会再执行检测。
    如果没有满足条件的元素，则返回false */
  const flag = state.visitedviews.some(v => v.path === view.path); //判断数组中是否已经存在该路由
  // 如果存在当前路由,则返回false,否则添加进去
  if (flag) {
    return false;
  } else {
    state.visitedviews.push({
      title: view.meta.title,
      path: view.path,
      closable: view.meta.closable,
    })
  }
};
//关闭页签--删除路由数据的方法
export const DEL_VISITED_VIEWS = (state, view) => {
  //entries() 方法返回一个数组的迭代对象，该对象包含数组的键值对 (key/value)。
  for (let [i, v] of state.visitedviews.entries()) {
    if (v.path === view.path) { //i代表索引,v代表对应的对象
      state.visitedviews.splice(i, 1);
      break
    }
  }
};
//关闭其他页签和关闭所有页签
export const BATCH_VISITED_VIEWS = (state, view) => {
  state.visitedviews = view;
};
//匹配过滤并获取首页信息及拥有的左侧权限
export const FILTER_ROUTER = (state, obj) => {
  if (obj.type === "home") {
    state.home = {
      path: obj.list.path,
      name: obj.list.name,
      meta: {
        title: obj.list.meta.title,
        closable: false
      }
    }
  } else {
    state[obj.type] = obj.list;
  }
};
//改变state中persistence的值
export const CHANGE_PERSISTENCE = (state, obj) => {
  state.persistence[obj.key] = obj.value;
};
