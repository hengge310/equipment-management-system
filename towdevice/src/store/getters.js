import {Flatten} from '../utils/common';
//获取首页
export const Home = state => {
  const list = Flatten(state.menus, "children", false);
  return {
    path: list[0].path,
    name: list[0].name,
    meta: {
      title: list[0].title,
      closable: false
    }
  };
};
