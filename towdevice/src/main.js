// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import store from "./store"; //引入store
import "./utils/permission";
// import './assets/css/reset.css'
import ElementUI from "element-ui"; //引入ElementUI
import "element-ui/lib/theme-chalk/index.css"; //引入ElementUI样式
import {
  getRequest,
  postRequest
} from "./utils/http";
import Viewer from "v-viewer"; //图片预览
import "viewerjs/dist/viewer.css";

import fullCalendar from "vue-fullcalendar";

Vue.component("full-calendar", fullCalendar);
// 引入vue-amap
import VueAMap from "vue-amap";
Vue.use(VueAMap);
// 初始化vue-amap
VueAMap.initAMapApiLoader({
  // 高德的key
  key: "760b639f05af6caac58d05900a892a80", //web端
  plugin: [
    "AMap.Autocomplete", //输入提示插件
    "AMap.PlaceSearch", //POI搜索插件
    "AMap.Scale", //右下角缩略图插件 比例尺
    "AMap.OverView", //地图鹰眼插件
    "AMap.ToolBar", //地图工具条
    "AMap.MapType", //类别切换控件，实现默认图层与卫星图、实施交通图层之间切换的控制
    "AMap.PolyEditor", //编辑 折线多，边形
    "AMap.CircleEditor", //圆形编辑器插件
    "AMap.Geolocation", //定位控件，用来获取和展示用户主机所在的经纬度位置
  ], //依赖配置，根据自己的需求引入
  v: "1.4.4",
  uiVersion: "1.0",
});
Vue.prototype.getRequest = getRequest; //http get请求
Vue.prototype.postRequest = postRequest; //http post请求
Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(Viewer); //图片预览

import VueScroller from 'vue-scroller'
Vue.use(VueScroller)

import echarts from "echarts";
Vue.prototype.$echarts = echarts;

import china from "echarts/map/json/china.json";
echarts.registerMap("china", china);

import formCreate from "@form-create/element-ui";
Vue.use(formCreate);

import EleUploadImage from "vue-ele-upload-image";
Vue.component(EleUploadImage.name, EleUploadImage);


window.onload = function () {
  // 禁用鼠标右键
  document.oncontextmenu = function () {
    window.event.returnValue = true;
  };
  // 禁止选择网页内容
  document.onselectstart = function () {
    window.event.returnValue = true;
  };
  //禁止复制
  document.oncopy = function () {
    window.event.returnValue = true;
  };
  //禁止拖动
  document.ondragstart = function () {
    window.event.returnValue = false;
  };
  // 禁用f10,f11,f12
  document.onkeydown = function () {
    const keyCode = window.event && window.event.keyCode;
    if (keyCode === 121) {
      //f10
      event.keyCode = 0;
      event.returnValue = false;
    }
    if (keyCode === 122) {
      //f11
      alert("想偷看代码,滚...");
      event.keyCode = 0;
      event.returnValue = false;
    }
    if (keyCode === 123) {
      //f12
      alert("想偷看代码,滚...");
      event.keyCode = 0;
      event.returnValue = false;
    }
  };
};

new Vue({
  el: "#app",
  router,
  store, //使用store
  components: {
    App,
  },
  template: "<App/>",
});
