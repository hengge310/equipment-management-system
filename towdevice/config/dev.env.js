'use strict';
const merge = require('webpack-merge');
const prodEnv = require('./prod.env');

module.exports = merge(prodEnv, { //开发环境
  NODE_ENV: '"development"',
  MAIN_ENV: '"/api"', //主接口域名
  UPLOAD: '"/Upload"', //图片上传域名
  kefu: '"/kefu"'
});
